**simple file manager**

My fork of [sfm](https://github.com/afify/sfm) patched to suit my needs.

Description
------------
sfm is a simple file manager for unix-like systems.
* pthreads(7) to read events, no timers.
* BSD kqueue(2) - kernel event notification mechanism.
* Linux inotify(7) - monitoring filesystem events.
* dual pane.
* bookmarks.
* open files by extension.
* bottom statusbar.
* vim-like key bindings.
* filter.
* no dependencies.
* c99 static linking.
* based on [termbox](https://github.com/nsf/termbox).
* Inspired by [vifm](https://vifm.info/) and [noice](https://git.2f30.org/noice/).
* Follows the suckless [philosophy](https://suckless.org/philosophy/).

Performance
------------
```sh
$ perf stat -r 10 sfm
```

Options
-------
```sh
$ sfm [-v]
$ man sfm
```

Run
---
```sh
$ sfm
```

Configuration
-------------
The configuration of sfm is done by creating a custom config.h
and (re)compiling the source code. This keeps it fast, secure and simple.
